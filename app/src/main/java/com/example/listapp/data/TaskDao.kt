package com.example.listapp.data

import androidx.room.*
import kotlinx.coroutines.flow.Flow


@Dao
interface TaskDao {

    fun getTasks(query: String, sortOrder: SortOrder, hideCompleted: Boolean): Flow<List<Task>> =
        when(sortOrder) {
            SortOrder.BY_DATE -> getTasksSorterByDateCreated(query, hideCompleted)
            SortOrder.BY_NAME -> getTasksSorterByName(query, hideCompleted)
        }

    @Query("SELECT * FROM task_table WHERE (completed != :hideCompleted OR completed = 0) AND  name like '%' || :searchQuery || '%' ORDER BY important Desc, name")
    fun getTasksSorterByName(searchQuery: String, hideCompleted: Boolean): Flow<List<Task>>


    @Query("SELECT * FROM task_table WHERE (completed != :hideCompleted OR completed = 0) AND  name like '%' || :searchQuery || '%' ORDER BY important Desc, created")
    fun getTasksSorterByDateCreated(searchQuery: String, hideCompleted: Boolean): Flow<List<Task>>

    @Query("SELECT * FROM task_table WHERE name like '%' || :searchQuery || '%' ORDER BY important Desc")
    fun getTasks(searchQuery: String): Flow<List<Task>>

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(task: Task)

    @Update
    suspend fun update(task: Task)

    @Delete
    suspend fun delete(task: Task)
}